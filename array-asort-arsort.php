<?php
	$arrNilai=array("dila"=>80, "adil"=>70, "aldi"=>75, "ladi"=>85);
	echo "<b>Array sebelum diurutkan</b>";
	echo "<pre>";
	print_r($arrNilai);
	echo "</pre>";

	asort($arrNilai);
	reset($arrNilai);
	echo "<b>Array setelah diurutkan dengan sort()</b>";
	echo "pre";
	print_r($arrNilai);
	echo "</pre>"; 

	arsort($arrNilai);
	reset($arrNilai);
	echo "<b>Array setelah diurutkan dengan rsort()</b>";
	echo "pre";
	print_r($arrNilai);
	echo "</pre>"; 
?>